import asyncio
from tornado.escape import json_decode
from tornado.ioloop import IOLoop
from utils import date_at_first_hour, date_at_midnigth, local_datetime, get_localzone
from models import Currency
from typing import Dict, List, Optional
from pymongo.collection import Collection
from core.errors import ExchangeApiError
import core
from datetime import date, datetime
from tornado.web import RequestHandler
from logging import Logger


class SummaryHandler(RequestHandler):
    logger: Logger
    collection: Collection
    base: str
    base_name: str

    def initialize(
        self, logger: Logger, collection: Collection, base: str, name: str
    ) -> None:
        setattr(self, "logger", logger)
        setattr(self, "base", base)
        setattr(self, "base_name", name)
        setattr(self, "collection", collection)

    async def ensure_currencies(self) -> List[Currency]:
        base, top = date_at_first_hour(local_datetime()), date_at_midnigth(
            local_datetime()
        )
        date_filter = {
            "date": {
                "$gte": base,
                "$lte": top,
            }
        }

        count = self.collection.count_documents(date_filter)

        if count > 0:
            self.logger.info("Summary found")
            return list(map(Currency.from_bson, self.collection.find(date_filter)))

        self.logger.warning("Summary not found, generating...")

        async def insert_all(currencies: List[Currency]) -> None:
            self.logger.debug("Generating %d new currencies", len(currencies))
            for currency in currencies:
                self.collection.insert_one(currency.as_bson())

        self.logger.info("Starting API Request")
        res = await core.get_exchanges(self.base.lower(), self.base_name)
        IOLoop.current().add_callback(insert_all, res)
        return res

    async def get(self) -> None:
        try:
            if self.get_query_argument("as_map", "off") == "on":
                res = dict()
            else:
                res = list()

            for cur in await self.ensure_currencies():
                if self.get_query_argument("as_map", "off") == "on":
                    res[cur.code.lower()] = cur.as_json()
                else:
                    res.append(cur.as_json())
        except ExchangeApiError as err:
            self.logger.warning(err)
            self.set_status(400)
            self.set_header("Content-Type", "application/json")
            self.finish({"error": {"name": "ApiError", "description": err.args}})
        except Exception as err:
            self.logger.error(err)
            self.set_status(500)
            self.set_header("Content-Type", "application/json")
            self.finish(
                {"error": {"name": err.__class__.__name__, "description": err.args}}
            )
        else:
            self.logger.info(
                "Generated map of %s with count of %d", self.base.upper(), len(res)
            )
            self.set_status(200)
            self.set_header("Content-Type", "application/json")
            self.finish({"currencies": res, "base": self.base.lower()})


class InfoHandler(RequestHandler):
    base: str
    version: str
    collection: Collection

    QUERY = [
        {"$sort": {"date": -1}},
        {
            "$set": {
                "date": {
                    "$dateToString": {
                        "date": "$date",
                        "format": "%Y-%m-%d",
                        "timezone": "America/Bogota",
                    }
                }
            }
        },
        {"$group": {"_id": "$date", "count": {"$sum": 1}}},
        {"$project": {"date": "$_id", "count": "$count"}},
        {"$unset": ["_id"]},
    ]

    def initialize(self, base: str, version: str, collection: Collection) -> None:
        setattr(self, "base", base)
        setattr(self, "version", version)
        setattr(self, "collection", collection)

    async def summary(self) -> List[Dict]:
        return list(self.collection.aggregate(self.QUERY + [{"$limit": 60}]))

    async def average(self) -> float:
        first = next(
            self.collection.aggregate(
                self.QUERY + [{"$group": {"_id": True, "avg": {"$avg": "$count"}}}]
            )
        )

        return first["avg"]

    async def get(self) -> None:
        summary, average = await asyncio.gather(self.summary(), self.average())

        self.set_header("Content-Type", "application/json")
        self.finish(
            {
                "version": f"SimpleExchange/{self.version}",
                "baseExchange": self.base,
                "summary": summary,
                "average": average,
            }
        )


class ConversorHandler(RequestHandler):
    logger: Logger
    collection: Collection

    def initialize(self, logger: Logger, collection: Collection) -> None:
        setattr(self, "logger", logger)
        setattr(self, "collection", collection)

    def find_currency(self, name: str, search_date: date = None) -> Optional[Currency]:
        if search_date is None:
            search_date = local_datetime().date()

        base = datetime(
            search_date.year,
            search_date.month,
            search_date.day,
            0,
            0,
            0,
            tzinfo=get_localzone(),
        )
        top = date_at_midnigth(base)

        currency_filter = {
            "code": name,
            "date": {
                "$gte": base,
                "$lte": top,
            },
        }

        res = self.collection.find_one(currency_filter)
        if res is not None:
            return Currency.from_bson(res)

        return None

    def post(self, base_currency: str, to_currency: str) -> None:
        search_date = None
        if self.get_query_argument("date", "") != "":
            str_search_date = self.get_query_argument("date")
            search_date = date.strftime(str_search_date, "%Y-%m-%d")

        base = self.find_currency(base_currency.upper(), search_date)
        if base is None:
            self.set_status(404)
            self.set_header("Content-Type", "application/json")
            return self.finish(
                {
                    "error": {
                        "name": "NotFound",
                        "description": f"the currency {base_currency} was not found",
                    }
                }
            )

        to = self.find_currency(to_currency.upper(), search_date)
        if to is None:
            self.set_status(404)
            self.set_header("Content-Type", "application/json")
            return self.finish(
                {
                    "error": {
                        "name": "NotFound",
                        "description": f"the currency {to_currency} was not found",
                    }
                }
            )

        content_type = self.request.headers.get_list("Content-Type")
        content_type = content_type[0] if len(content_type) > 0 else "nobody"
        if content_type != "application/json":
            self.set_status(500)
            self.set_header("Content-Type", "application/json")
            return self.finish(
                {
                    "error": {
                        "name": "ParseError",
                        "description": f'MIME Type "{content_type}" has no parser',
                    }
                }
            )


        data = json_decode(self.request.body)
        if not isinstance(data, (int, float)) and (
            isinstance(data, dict) and "value" not in data
        ):
            self.set_status(400)
            self.set_header("Content-Type", "application/json")
            return self.finish(
                {
                    "error": {
                        "name": "RuntimeError",
                        "description": 'you must provide a number or a dict with a numeric value in "value"',
                    }
                }
            )
        elif isinstance(data, dict) and not isinstance(data["value"], (int, float)):
            self.set_status(400)
            self.set_header("Content-Type", "application/json")
            return self.finish(
                {
                    "error": {
                        "name": "RuntimeError",
                        "description": 'the field "value" must be a number',
                    }
                }
            )

        value = data if isinstance(data, (int, float)) else data["value"]
        result = round((base.inverse_rate * value) * to.rate, 4)

        self.set_status(200)
        self.set_header("Content-Type", "application/json")
        self.finish({"value": result})
