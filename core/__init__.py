from core.errors import ExchangeApiError
from tornado.httpclient import AsyncHTTPClient
from typing import List
from models import Currency
from utils import local_datetime
import json


async def get_exchanges(base: str, base_name: str = "") -> List[Currency]:
    cli = AsyncHTTPClient()

    res = await cli.fetch(f"http://www.floatrates.com/daily/{base}.json", False)
    if res.code == 403:
        raise ExchangeApiError("Api not found the currency")
    elif res.code != 200:
        raise ExchangeApiError("Api return a non-200 response")
    elif res.error is not None:
        raise res.error

    currencies = list()
    date = local_datetime()
    currencies.append(Currency(code=base.upper(), name=base_name, rate=1, date=date))
    for base_currency in json.loads(res.body).values():
        currencies.append(
            Currency(
                code=base_currency["alphaCode"],
                name=base_currency["name"],
                rate=base_currency["rate"],
                date=date,
            )
        )

    return currencies
