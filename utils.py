from datetime import datetime
from tzlocal import get_localzone
from pytz import UTC


def local_datetime() -> datetime:
    return datetime.now().replace(tzinfo=get_localzone())


def date_at_first_hour(date: datetime) -> datetime:
    return date.replace(hour=0, minute=0, second=0, microsecond=0)


def date_at_midnigth(date: datetime) -> datetime:
    return date.replace(hour=23, minute=59, second=59, microsecond=0)


def utc_to_localzone(date: datetime) -> datetime:
    return date.replace(tzinfo=UTC).astimezone(get_localzone())


def get_tzname() -> str:
    return get_localzone().tzname() or "UTC"
