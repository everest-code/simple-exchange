from __future__ import annotations
from dataclasses import dataclass, field
from abc import ABC, abstractmethod
from typing import Any, Dict, List, Optional, Union
from bson import ObjectId
from datetime import datetime
from utils import local_datetime, utc_to_localzone


class JSONSerializable(ABC):
    """
    Interface where create a serializable dict to send in JSON
    """

    @abstractmethod
    def as_json(self) -> Optional[Union[List, Dict, str, int, bool]]:
        pass


class MongoSerializable(ABC):
    """
    Interface to create a MongoDB serializable object and serialize from MongoDB
    """

    @abstractmethod
    def as_bson(self) -> Optional[Union[List, Dict, str, int, bool]]:
        pass

    @staticmethod
    @abstractmethod
    def from_bson(obj: Dict) -> Any:
        pass


@dataclass
class Currency(JSONSerializable, MongoSerializable):
    """
    Represantation of currency in Python with the base setted on `config.toml`
    """

    _id: ObjectId = field(repr=False, init=False)
    code: str
    name: str
    rate: float = field(repr=False)
    date: datetime = field(
        repr=False,
        default_factory=local_datetime,
    )

    @property
    def inverse_rate(self) -> float:
        return 1 / self.rate

    def as_json(self) -> Optional[Union[List, Dict, str, int, bool]]:
        return {
            "code": self.code,
            "name": self.name,
            "creationDate": self.date.isoformat(),
            "rate": {
                "normal": self.rate,
                "inverse": self.inverse_rate,
            },
        }

    def as_bson(self) -> Optional[Union[List, Dict, str, int, bool]]:
        obj = {
            "code": self.code,
            "date": self.date,
            "name": self.name,
            "rate": self.rate,
        }

        if getattr(self, "_id", None) is not None:
            obj["_id"] = self._id

        return obj

    @staticmethod
    def from_bson(obj: Dict) -> Currency:
        c_obj = obj.copy()
        _id = c_obj["_id"]
        del c_obj["_id"]

        c_obj["date"] = utc_to_localzone(c_obj["date"])

        inst = Currency(**c_obj)
        inst._id = _id

        return inst
