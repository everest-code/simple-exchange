from functools import cache
from tornado.web import Application, GZipContentEncoding
from config import config
from logging import getLogger, basicConfig
from db import create_client
from routes.api import *
from tasks import TaskConfig, compile as tr
from tasks.tasks import *

basicConfig(
    level=config()["log_level"],
    format="[%(asctime)s | %(levelname)s][%(name)s]: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S %Z",
)

log = getLogger("server")


@cache
def get_collection() -> Collection:
    log.info("Database connected")
    collection = create_client(config()["db"])["exchange"]

    return collection


def make_tasks() -> List[TaskConfig]:
    logger = getLogger("server.tasks")
    collection = get_collection()
    return [
        TaskConfig(
            fx=update_exchanges,
            ctx=dict(**config()["exchange"], logger=logger, collection=collection),
            second=tr(r"^0$"),
            minute=tr(r"^0$"),
            hour=tr(r"^(0|12)$"),
        )
    ]


def make_app() -> Application:
    log.info("Server was created")

    collection = get_collection()
    logger = getLogger("server.routes")

    return Application(
        [
            (
                r"^/info",
                InfoHandler,
                dict(
                    version="0.0.1",
                    base=config()["exchange"]["base"],
                    collection=collection,
                ),
            ),
            (
                r"^/summary",
                SummaryHandler,
                dict(**config()["exchange"], collection=collection, logger=logger),
            ),
            (
                r"^/convert/([a-zA-Z]{3})/([a-zA-Z]{3})",
                ConversorHandler,
                dict(collection=collection, logger=logger),
            ),
        ],
        transforms=[GZipContentEncoding],
    )
