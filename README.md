# Simple Exchange

A simple API with tornado and MongoDB, serialize the data from [Float Rates](http://www.floatrates.com) and make into an easy use API.

View on [Docker Hub](https://hub.docker.com/r/sgt911/simple-exchange)
