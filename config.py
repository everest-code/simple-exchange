import toml
from os import environ
from functools import cache


@cache
def config() -> dict:
    filename = environ.get("CONFIG", "./config.toml")
    with open(filename, "r") as file:
        return toml.loads(file.read())
