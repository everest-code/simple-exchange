#!/usr/bin/env python

from server import make_app, make_tasks
from tasks import start_runner
from tornado.ioloop import IOLoop
from config import config

if __name__ == "__main__":
    # Creating task and server
    tasks = make_tasks()
    app = make_app()

    # Enabling HTTP Server
    if config()["server"]["production_deploy"]:
        from tornado.httpserver import HTTPServer

        server = HTTPServer(app)
        server.bind(config()["server"]["port"], "0.0.0.0")
        server.start(config()["server"]["proccess_count"])
    else:
        app.listen(config()["server"]["port"])

    # Enabling Anacron Runner
    IOLoop.current().add_callback(start_runner, tasks)

    # IOLoop starting
    try:
        IOLoop.current().start()
    except KeyboardInterrupt:
        IOLoop.instance().stop()
