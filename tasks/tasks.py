from logging import Logger, log
from typing import Dict, List
from pymongo.collection import Collection
from tornado.ioloop import IOLoop
from utils import local_datetime, date_at_first_hour, date_at_midnigth
from models import Currency
import core


async def update_exchanges(ctx: Dict) -> None:
    date_filter = {
        "date": {
            "$gte": date_at_first_hour(local_datetime()),
            "$lte": date_at_midnigth(local_datetime()),
        }
    }

    coll: Collection = ctx["collection"]
    logger: Logger = ctx["logger"]
    if coll.count_documents(date_filter) > 0:
        logger.debug("DB already populated ignoring")
        return None

    logger.debug("Populating DB")
    ioloop: IOLoop = ctx["ioloop"]
    currencies = await core.get_exchanges(ctx["base"], ctx["name"])

    async def insert_all(currencies: List[Currency]) -> None:
        logger.info("Generating %d new currencies", len(currencies))
        for currency in currencies:
            coll.insert_one(currency.as_bson())

    ioloop.add_callback(insert_all, currencies)
