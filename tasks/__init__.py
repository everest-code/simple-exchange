from asyncio import sleep, gather
from asyncio.futures import Future
from tornado.ioloop import IOLoop
from functools import wraps
from typing import Callable, Coroutine, List, Tuple, Union
from dataclasses import dataclass, field
from datetime import datetime
from re import Pattern
from utils import local_datetime
from logging import DEBUG, getLogger
import os, psutil
from re import compile

log = getLogger("tasks")
log_runner = getLogger("tasks.runner")


def default_time_pattern() -> Pattern:
    return compile(r"\d+")


@dataclass
class TaskConfig:
    """
    Configuration of AsyncIO task runner, like a CronJob
    """

    fx: Callable[[dict], Union[Coroutine, None]]
    ctx: dict = field(default_factory=dict, repr=False)
    second: Pattern = field(default_factory=default_time_pattern, repr=False)
    minute: Pattern = field(default_factory=default_time_pattern, repr=False)
    hour: Pattern = field(default_factory=default_time_pattern, repr=False)
    day: Pattern = field(default_factory=default_time_pattern, repr=False)
    month: Pattern = field(default_factory=default_time_pattern, repr=False)
    year: Pattern = field(default_factory=default_time_pattern, repr=False)
    running: bool = field(default=False, init=False, repr=True)
    has_error: bool = field(default=False, init=False, repr=True)

    def can_run(self, date: datetime) -> bool:
        rules = [
            self.second.match(str(date.second)),
            self.minute.match(str(date.minute)),
            self.hour.match(str(date.hour)),
            self.day.match(str(date.day)),
            self.month.match(str(date.month)),
            self.year.match(str(date.year)),
        ]

        return None not in rules

    def as_task(self) -> Callable[[], Coroutine]:
        @wraps(self.fx)
        async def wrapper() -> bool:
            if self.can_run(local_datetime()):
                if self.has_error:
                    log.warning("Ignoring has an error %r", self)
                    return True

                if self.running:
                    log.debug("Ignoring already running %r", self)
                    return True

                log.info("Running %r", self)
                try:
                    ctx = self.ctx.copy()
                    if "logger" not in self.ctx:
                        ctx.update({"logger": log_runner})
                    if "ioloop" not in self.ctx:
                        ctx.update({"ioloop": IOLoop.current()})

                    self.running = True
                    _ret = self.fx(ctx)
                    if _ret is not None:
                        log.debug("Awaiting %r", self)
                        await _ret
                    log.debug("Complete %r", self)
                except Exception as ex:
                    self.has_error = True
                    log.exception("Error with %r: %s", self, ex)
                    return False
                else:
                    self.running = False
                    return True

            log.debug("%r not scheduled", self)
            return False

        return wrapper


async def start_runner(tasks: List[TaskConfig]) -> Coroutine:
    if os.path.isfile("/tmp/simple_exchange.runner.pid"):
        with open("/tmp/simple_exchange.runner.pid", "r") as file:
            pid = int(file.read())
            if psutil.pid_exists(pid) and pid != os.getpid():
                log_runner.debug("Task runner already active")
                return None

    with open("/tmp/simple_exchange.runner.pid", "w") as file:
        file.write(str(os.getpid()))

    log_runner.info("Task runner forked on %d", os.getpid())
    runner_tasks = [task.as_task() for task in tasks]
    log_runner.info("Starting %d tasks", len(runner_tasks))

    async def await_to_finish(tasks: Future[Tuple[bool]]):
        res = await tasks
        if log_runner.isEnabledFor(DEBUG):
            total = len(res)
            ok = res.count(True)
            log_runner.debug("Completed %d/%d tasks", ok, total)

    while True:
        await sleep(1)
        gather_tasks = gather(*[task() for task in runner_tasks])
        IOLoop.current().add_callback(await_to_finish, gather_tasks)
