from pymongo import MongoClient
from pymongo.database import Database


def create_client(conf: str) -> Database:
    return getattr(MongoClient(conf['dsn']), conf['database'])
